import { useState } from 'react';
import { useDebounceFn } from 'ahooks';

import styles from './styles.module.css';

export default function LikeButton(){
  const [liked, setLiked] = useState(false);
  const [offsetTimer, setOffsetTimer] = useState(1000);
  
  const {run: sendRequest} = useDebounceFn((status) => {
    console.log(`Request sent! The user liked is ${status}`);
  },offsetTimer);

  const handleClick = () => {
    setLiked(!liked);
    sendRequest(liked);
  }

  return(
    <button className={`${styles.likeButton} ${liked ? styles.reverse : ''}`} onClick={handleClick} style={{'--mainColor':'#f0f'}}>
      &#128078;
    </button>
  );
}