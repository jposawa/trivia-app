import React, {createContext, useState, useContext} from 'react';

export const ControlContext = createContext();

export const ControlProvider = ({children}) => {
  const CONFIG = {
    LOCALSTORAGE_PREFIX: "jptrivia@",
  }
  const [currentQuestion, setCurrentQuestion] = useState();

  const exportedValues = {
    CONFIG,
    currentQuestion,
  };

  return (
    <ControlContext.Provider value={exportedValues}>
      {children}
    </ControlContext.Provider>
  );
}

export const useControl = () =>{
  const content = useContext(ControlContext);

  if(!content){
    console.log("It need to be inside a Provider");
  }

  return content;
}